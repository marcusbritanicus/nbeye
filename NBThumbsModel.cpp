/*
	*
	* This file is a part of CoreFM.
	* A file manager for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


// Local Headers
#include "NBThumbsModel.hpp"

/* ##### Thumbnailer Class ========================================================================================================== */
Thumbnailer::Thumbnailer( QObject *parent ) : QThread( parent ) {

	fileList.clear();
	isActive = false;
};

void Thumbnailer::acquire( QString filename ) {

	if ( not fileList.contains( filename ) )
		fileList << filename;

	if ( not isActive )
		start();
};

void Thumbnailer::run() {

	while ( true ) {
		if ( fileList.count() ) {
			isActive = true;

			QString filename = fileList.takeFirst();
			if ( getThumb( filename ) )
				emit updateItem( filename );
		}

		else {
			isActive = false;
			return;
		}
	}
};

bool Thumbnailer::getThumb( QString item ) {

	// Thumbnail image
	QImage pic;
	QPixmap thumb( QSize( 128, 128 ) );
	thumb.fill( Qt::transparent );

	QImageReader picReader( item );

	pic = picReader.read();
	if ( pic.isNull() ) {
		qDebug() << "Bad image";
		return false;
	}

	pic = pic.scaled( 512, 512, Qt::KeepAspectRatio, Qt::FastTransformation );
	pic = pic.scaled( 128, 128, Qt::KeepAspectRatio, Qt::SmoothTransformation );

	QPainter painter( &thumb );
	painter.drawImage( QRectF( QPointF( ( 128 - pic.width() ) / 2, ( 128 - pic.height() ) / 2 ), QSizeF( pic.size() ) ), pic );
	painter.end();

	NBThumbsModel::iconMap.insert( item, thumb );
	return true;
};

/* ##### File System Model ========================================================================================================== */

QStringList NBThumbsModel::supportedFormats = QStringList();
QHash<QString, QIcon> NBThumbsModel::iconMap = QHash<QString, QIcon>();

NBThumbsModel::NBThumbsModel( QWidget *parent ) : QFileSystemModel( parent ) {

	QStringList nameFilterList;
	Q_FOREACH( QByteArray imgFmt, QImageReader::supportedImageFormats() ) {
		supportedFormats << QString::fromLatin1( imgFmt ).toLower();
		nameFilterList << "*." + QString::fromLatin1( imgFmt ).toLower();
	}

	setFilter( QDir::Files | QDir::NoDotAndDotDot );
	setNameFilters( nameFilterList );
	setNameFilterDisables( false );

	thumbnailer = new Thumbnailer( this );
	connect( thumbnailer, SIGNAL( updateItem( QString ) ), this, SLOT( updateItem( QString ) ) );
};

NBThumbsModel::~NBThumbsModel() {

	thumbnailer->terminate();
	thumbnailer->deleteLater();
};

QVariant NBThumbsModel::data( const QModelIndex &idx, int role ) const {

	switch ( role ) {
		case Qt::DisplayRole : {

			return qApp->fontMetrics().elidedText( QFileSystemModel::data( idx, Qt::DisplayRole ).toString(), Qt::ElideMiddle, 128 );
			return QString();
		}

		case Qt::DecorationRole : {

			if ( idx.column() != 0 )
				return QIcon();

			QString fileName = QFileSystemModel::data( idx, Qt::DisplayRole ).toString();
			if ( supportedFormats.contains( fileName.split( "." ).last().toLower() ) ) {

				/* If thumbnail is not acquired, acquire it */
				if ( not iconMap.contains( rootPath() + "/" + fileName ) )
					thumbnailer->acquire( rootPath() + "/" + fileName );

				/* Acquired thumbnail */
				else
					return iconMap.value( rootPath() + "/" + fileName );
			}

			return QIcon::fromTheme( "image-x-generic" );
		}

		default: {

			return QFileSystemModel::data( idx, role );
		}
	}
};

void NBThumbsModel::reload() {

	setFilter( QDir::Files | QDir::NoDotAndDotDot );
	setNameFilters( supportedFormats );

	setRootPath( rootPath() );
};

void NBThumbsModel::updateItem( QString filename ) {

	QModelIndex idx = QFileSystemModel::index( filename );
	emit updateItem( idx );
};
