/*
    *
    * This file is a part of NBEye.
    * NBEye is an Image Viewer based on Qt5
    * Copyright 2019 Marcus Britanicus <marcusbritanicus@gmail.com>
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "NBEyeView.hpp"
#include "NBThumbsView.hpp"

class NBEye : public QMainWindow {
	Q_OBJECT

	public:
		NBEye();

	private:
		void createUI();
		void setWindowProperties();

		NBEyeView *eyeView;
		NBThumbsView *thumbsView;
		QLabel *nameLbl;
};
