/*
	*
	* This file is a part of CoreImage.
	* An image viewer for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 2 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "NBThumbsView.hpp"

NBThumbsView::NBThumbsView( QWidget *parent ) : QListView( parent ) {

	setFlow( QListView::LeftToRight );
	setViewMode( QListView::IconMode );
	setMovement( QListView::Static );
	setResizeMode( QListView::Adjust );
	setLayoutMode( QListView::SinglePass );
	setSelectionMode( QAbstractItemView::SingleSelection );

	setIconSize( QSize( 128, 128 ) );
	setGridSize( QSize( 150, 150 ) );
	setFrameStyle( QFrame::NoFrame );

	fsm = new NBThumbsModel( this );
	setModel( fsm );
	connect( this, &QListView::clicked, this, &NBThumbsView::handleClicked );
	connect( selectionModel(), &QItemSelectionModel::currentChanged, this, &NBThumbsView::handleChanged );

	connect( fsm, qOverload<QModelIndex>(&NBThumbsModel::updateItem), this, &NBThumbsView::updateItem );
};

void NBThumbsView::setRootPath( QString path ) {

	setRootIndex( fsm->setRootPath( path ) );
};

void NBThumbsView::nextImage() {

	int next = currentIndex().row() + 1;
	if ( next == fsm->rowCount( rootIndex() ) )
		next = 0;

	handleClicked( fsm->index( next, 0, rootIndex() ) );
};

void NBThumbsView::previousImage() {

	int previous = currentIndex().row() - 1;
	if ( previous == -1 )
		previous = fsm->rowCount( rootIndex() ) - 1;

	handleClicked( fsm->index( previous, 0, rootIndex() ) );
};

void NBThumbsView::handleClicked( const QModelIndex &curIdx ) {

	emit loadImage( fsm->filePath( curIdx ) );
};

void NBThumbsView::handleChanged( const QModelIndex &curIdx, const QModelIndex & ) {

	emit loadImage( fsm->filePath( curIdx ) );
};

void NBThumbsView::paintEvent( QPaintEvent *pEvent ) {

	QPainter painter( viewport() );
	painter.setPen( Qt::red );
	painter.drawLine( rect().topRight(), rect().bottomRight() );
	painter.end();

	QListView::paintEvent( pEvent );
};

void NBThumbsView::updateItem( QModelIndex idx ) {

	QAbstractItemView::dataChanged( idx, idx, QVector<int>() << Qt::DecorationRole );
};
