/*
	*
	* This file is a part of CoreImage.
	* An image viewer for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 2 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QtWidgets>
#include "NBThumbsModel.hpp"

class NBThumbsView : public QListView {

	Q_OBJECT

	public:
		NBThumbsView( QWidget *parent = nullptr );
		void setRootPath( QString );

		void nextImage();
		void previousImage();

	private:
		NBThumbsModel *fsm;

		void handleClicked( const QModelIndex &current );
		void handleChanged( const QModelIndex &, const QModelIndex & );
		void updateItem( QModelIndex );

	Q_SIGNALS:
		void loadImage( QString );

	protected:
		void paintEvent( QPaintEvent * );
};
