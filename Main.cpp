/*
    *
    * This file is a part of NBEye.
    * NBEye is an Image Viewer based on Qt5
    * Copyright 2019 Marcus Britanicus <marcusbritanicus@gmail.com>
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include "NBEye.hpp"

#include <QApplication>
#include <QFileInfo>
#include <QCommandLineParser>

int main( int argc, char *argv[] ) {

    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed( true );

    // Set application info
    app.setOrganizationName( "NB" );
    app.setApplicationName( "NBEye" );
    app.setApplicationVersion( "1.0.0" );

    NBEye eye;
    eye.show();

    return app.exec();
};
