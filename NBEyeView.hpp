/*
	*
	* This file is a part of CoreImage.
	* An image viewer for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 2 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QtWidgets>

class NBEyeView : public QGraphicsView {

	Q_OBJECT

	public:
		NBEyeView( QWidget *parent = nullptr );
		void loadImage( QString );
		void clear();
		QImage asImage();

		qreal zoomFactor();

	public slots:
		void zoomIn();
		void zoomOut();
		void zoomNormal();
		void zoomFit();

		void rotateLeft();
		void rotateRight();

		bool saveImage( QString filename = QString() );

	private:
		const double scaleFactor = 1.05;
		const double invScaleFactor = 1 / 1.05;

		bool isFit = false;
		QString mFileName;

		QGraphicsPixmapItem *image;
		QGraphicsScene *mScene;

		qreal mScale = 1.0;

	protected:
		void mouseDoubleClickEvent( QMouseEvent *event );
		void resizeEvent( QResizeEvent *rEvent );
		void wheelEvent( QWheelEvent *wEvent );

	signals:
		void nextImage();
		void prevImage();
		void zoomChanged();

		void imageChanged( QString name );
};
