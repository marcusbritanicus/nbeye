/*
	*
	* This file is a part of CoreImage.
	* An image viewer for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 2 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "NBEyeView.hpp"

NBEyeView::NBEyeView( QWidget *parent ) : QGraphicsView( parent ) {

	setRenderHints( QPainter::HighQualityAntialiasing | QPainter::Antialiasing | QPainter::TextAntialiasing );
	setCacheMode( QGraphicsView::CacheBackground );
	setViewportUpdateMode( QGraphicsView::BoundingRectViewportUpdate );

	setBackgroundBrush( Qt::black );
	setFrameStyle( QFrame::NoFrame );

	image = new QGraphicsPixmapItem();
	image->setTransformationMode( Qt::SmoothTransformation );

	mScene = new QGraphicsScene();
	mScene->addItem( image );

	setScene( mScene );
};

void NBEyeView::loadImage( QString fileName ) {

	mFileName = fileName;

	QPixmap pix( fileName );

	if ( pix.isNull() )
		pix = QIcon::fromTheme( "image-x-generic" ).pixmap( 256 );

	image->setPixmap( pix );
	mScene->setSceneRect( image->boundingRect() );

	emit imageChanged( QFileInfo( fileName ).baseName() );

	QSize pxSize = pix.size();
	QSize vwSize = viewport()->size();

	if ( ( pxSize.width() > vwSize.width() ) or ( pxSize.height() > vwSize.height() ) )
		zoomFit();
};

void NBEyeView::clear() {

	isFit = false;
};

QImage NBEyeView::asImage() {

	return image->pixmap().toImage();
};

qreal NBEyeView::zoomFactor() {

	return mScale;
};

void NBEyeView::zoomIn() {

	scale( scaleFactor, scaleFactor );
	isFit = false;

	mScale *= scaleFactor;
	zoomChanged();
};

void NBEyeView::zoomOut() {

	scale( invScaleFactor, invScaleFactor );
	isFit = false;

	mScale *= invScaleFactor;
	zoomChanged();
};

void NBEyeView::zoomNormal() {

	resetMatrix();
	isFit = false;

	mScale = 1.0;
	zoomChanged();
};

void NBEyeView::zoomFit() {

	fitInView( image, Qt::KeepAspectRatio );
	isFit = true;

	/* matrix() returns the 2D scaling matrix */
	mScale = matrix().m11();

	zoomChanged();
};

void NBEyeView::rotateLeft() {

	QTransform tran;
	tran.rotate( -90 );

	mScene->removeItem( image );
	image->setPixmap( image->pixmap().transformed( tran, Qt::SmoothTransformation ) );
	mScene->addItem( image );
	mScene->setSceneRect( image->boundingRect() );

	if ( isFit )
		zoomFit();
};

void NBEyeView::rotateRight() {

	QTransform tran;
	tran.rotate( 90 );

	mScene->removeItem( image );
	image->setPixmap( image->pixmap().transformed( tran, Qt::SmoothTransformation ) );
	mScene->addItem( image );
	mScene->setSceneRect( image->boundingRect() );

	if ( isFit )
		zoomFit();
};

bool NBEyeView::saveImage( QString fileName ) {

	if ( fileName.count() )
		mFileName = fileName;

	return image->pixmap().save( mFileName );
};

void NBEyeView::mouseDoubleClickEvent( QMouseEvent *event ) {

	if ( event->button() == Qt::LeftButton ) {
		if ( isFit )
			zoomNormal();

		else
			zoomFit();
	}
};

void NBEyeView::resizeEvent( QResizeEvent *rEvent ) {

	rEvent->accept();

	if ( isFit )
		zoomFit();
};

void NBEyeView::wheelEvent( QWheelEvent *wEvent ) {

	int angle = wEvent->angleDelta().y();

	if ( qApp->keyboardModifiers() & Qt::ControlModifier ) {
		if ( angle > 0 ) {

			zoomIn();
		}

		else if ( angle < 0 ) {

			zoomOut();
		}
	}

	else {
		if ( angle > 0 ) {

			emit prevImage();
		}

		else if ( angle < 0 ) {

			emit nextImage();
		}
	}

	wEvent->accept();
};
