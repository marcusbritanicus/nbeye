/*
    *
    * This file is a part of NBEye.
    * NBEye is an Image Viewer based on Qt5
    * Copyright 2019 Marcus Britanicus <marcusbritanicus@gmail.com>
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include "NBEye.hpp"

NBEye::NBEye() : QMainWindow() {

	createUI();
	setWindowProperties();

	/* Load the first image */
	if ( qApp->arguments().count() > 1 ) {
		eyeView->loadImage( qApp->arguments().value( 1 ) );
		thumbsView->setRootPath( QFileInfo( qApp->arguments().value( 1 ) ).absolutePath() );
	}

	else {
		QString filename;
		do {
			filename = QFileDialog::getOpenFileName(
				this,
				"NBEye - Load Image",
				QDir::homePath(),
				QString( "Images (*." ) + NBThumbsModel::supportedFormats.join( " *." ) + ")"
			);
		} while ( filename.isEmpty() );

		eyeView->loadImage( filename );
		thumbsView->setRootPath( QFileInfo( filename ).absolutePath() );
	}
};

void NBEye::createUI() {

	thumbsView = new NBThumbsView( this );
	thumbsView->setFixedWidth( 200 );

	nameLbl = new QLabel( "NBEye - A simple Qt5 Image Viewer" );
	nameLbl->setAlignment( Qt::AlignCenter );
	nameLbl->setStyleSheet(  "background: black; color: white;");

	eyeView = new NBEyeView( this );
	connect( thumbsView, &NBThumbsView::loadImage, eyeView, &NBEyeView::loadImage );
	connect( eyeView, &NBEyeView::nextImage, thumbsView, &NBThumbsView::nextImage );
	connect( eyeView, &NBEyeView::prevImage, thumbsView, &NBThumbsView::previousImage );
	connect( eyeView, &NBEyeView::imageChanged, nameLbl, &QLabel::setText );

	QGridLayout *lyt = new QGridLayout();
	lyt->setContentsMargins( QMargins() );
	lyt->setSpacing( 0 );

	lyt->addWidget( thumbsView, 0, 0, 2, 1 );
	lyt->addWidget( nameLbl, 0, 1 );
	lyt->addWidget( eyeView, 1, 1 );

	QWidget *base = new QWidget( this );
	base->setLayout( lyt );
	setCentralWidget( base );
};

void NBEye::setWindowProperties() {

	setWindowTitle( "NBEye" );
	setWindowIcon( QIcon( ":/nbeye.png" ) );

	setWindowFlags( Qt::Window | Qt::FramelessWindowHint );
	setAttribute( Qt::WA_TranslucentBackground, true );
	setFixedSize( qApp->primaryScreen()->size() );
};
