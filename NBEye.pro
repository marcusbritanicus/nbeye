TEMPLATE = app
TARGET = nbeye

QT += widgets

INCLUDEPATH += .
DEPENDPATH += .

# Input
HEADERS += NBEye.hpp
HEADERS += NBEyeView.hpp
HEADERS += NBThumbsView.hpp
HEADERS += NBThumbsModel.hpp

SOURCES += Main.cpp
SOURCES += NBEye.cpp
SOURCES += NBEyeView.cpp
SOURCES += NBThumbsView.cpp
SOURCES += NBThumbsModel.cpp

# Build section
BUILD_PREFIX = $$(BUILD_DIR)

MOC_DIR       = $$BUILD_PREFIX/NBEye/moc
OBJECTS_DIR   = $$BUILD_PREFIX/NBEye/obj
RCC_DIR	      = $$BUILD_PREFIX/NBEye/qrc
UI_DIR        = $$BUILD_PREFIX/NBEye/uic

CONFIG += silent
